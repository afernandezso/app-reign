import { Model } from 'mongoose';
import { Process, ProcessModel } from 'src/api/repository/mongoDB/entity/process.interface';
import {Injectable, Logger} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

/**
 * class to access mongodb
 */
@Injectable()
export class ProcessRepositoryService {
  constructor(
    @InjectModel('data') private readonly processModel: Model<ProcessModel>,
  ) {}

  /**
   * method to create a record
   */
  async create(process: Process): Promise<Process> {
    const isProcess = await this.findById(process.objectID);
    if (!isProcess) {
      const newPost = new this.processModel(process);
      return newPost.save();
    }else{
      Logger.debug(`the id ${process.objectID} is already registered`);
    }
  }

  /**
   * method to get all data
   */
  async getAll(): Promise<Process[]> {
    return await this.processModel.find({ state: true });
  }

  /**
   * method to get a record by id
   */
  async findById(id: string): Promise<Process> {
    return await this.processModel.findOne({ objectID: id });
  }

  /**
   * mmethod to delete a record by id
   */
  async delete(id: string): Promise<Process> {
    const deletedProcess = await this.findById(id);
    if (deletedProcess) {
      deletedProcess.state = false;
      const newPost = new this.processModel(deletedProcess);
      return newPost.save();
    }else{
      Logger.debug(`the id ${id} is not registered`);
    }
  }

  /**
   * method to clean mongodb
   */
  async deleteMany() {
    return await this.processModel.deleteMany({});
  }
}
