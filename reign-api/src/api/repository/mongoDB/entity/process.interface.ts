import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ProcessModel = Process & Document;
/**
 * Process schema
 */

@Schema()
export class Process {
  @Prop({ required: true })
  objectID: string;
  @Prop({ required: true })
  title: string;
  @Prop({ required: true })
  author: string;
  @Prop({ required: true })
  url: string;
  @Prop({ required: true })
  createdAt: string;
  @Prop({ required: true })
  state: boolean;
}
export const ProcessSchema = SchemaFactory.createForClass(Process);
