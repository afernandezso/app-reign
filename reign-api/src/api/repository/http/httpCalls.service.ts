import { Injectable, HttpService, Logger } from '@nestjs/common';
import { map } from 'rxjs/operators';

/**
 *  class for http calls
 */
@Injectable()
export class HttpCallsService {
  constructor(private http: HttpService) {}

  /**
   *method to get data from hn.algolia.com
   */
  async getDataAlgolia() {
    Logger.debug('HttpCallsService getDataAlgolia()');
    return this.http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(map((response) => response.data));
  }
}
