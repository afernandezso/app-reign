import { Test, TestingModule } from '@nestjs/testing';
import { HttpCallsService } from './httpCalls.service';
import { of } from 'rxjs';
import { HttpService } from '@nestjs/common';

describe('HttpCallsService', () => {
  let service: HttpCallsService;
  const mockHttpService = {};
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HttpCallsService,
        { provide: HttpService, useValue: mockHttpService },
      ],
    }).compile();

    service = module.get<HttpCallsService>(HttpCallsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
