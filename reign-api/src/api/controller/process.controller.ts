import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  Res,
  ServiceUnavailableException,
} from '@nestjs/common';
import { ProcessService } from '../service/process/process.service';
import { ApiError } from '../error/api.error';
import { Process } from '../repository/mongoDB/entity/process.interface';

/**
 *  controller for all api processes
 */
@Controller('process')
export class ProcessController {
  constructor(private readonly processService: ProcessService) {}

  /**
   * method to get all data loaded
   * return a response with httpStatus and json
   */
  @Get('/')
  @HttpCode(HttpStatus.OK)
  async getAll(): Promise<Process[]> {
    Logger.debug('ProcessController getAll()');
    try {
      const processList = await this.processService.getAll();
      return processList;
    } catch (error) {
      Logger.error('Error getAll()', error);
      new InternalServerErrorException(new ApiError(error.message));
    }
  }

  /**
   * method to get a process pot id
   * return a response with httpStatus and json
   */
  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  async findById(@Param('id') id: string): Promise<Process> {
    let processById;
    try {
      console.log('ProcessController findById() id ', id);
      processById = await this.processService.findById(id);
    } catch (error) {
      error.htt;
      Logger.error('Error findById()', error);
      new ServiceUnavailableException(new ApiError(error.message));
    }
    if (!processById) {
      new NotFoundException(new ApiError(`id: ${id} not found`));
    }
    return processById;
  }

  /**
   * method to remove a process pot id
   * return a response with httpStatus and json
   */
  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  async delete(@Param('id') id: string): Promise<Process> {
    let processDelete;
    try {
      Logger.debug(`ProcessController delete() id: ${id}`);
      processDelete = await this.processService.delete(id);
    } catch (error) {
      Logger.error('Error delete()', error);
      new ServiceUnavailableException(new ApiError(error.message));
    }
    if (!processDelete) {
      new NotFoundException(new ApiError(`id: ${id} not found`));
    }

    return processDelete;
  }

  /**
   * method to remove all processes from the db
   * was implemented only to delete test data
   */
  @Delete('/')
  @HttpCode(HttpStatus.OK)
  async deleteMany() {
    try {
      console.log('ProcessController deleteMany()');
      const responseAll = await this.processService.deleteMany();
      return responseAll;
    } catch (error) {
      Logger.error('Error deleteMany()', error);
      new ServiceUnavailableException(new ApiError(error.message));
    }
  }
}
