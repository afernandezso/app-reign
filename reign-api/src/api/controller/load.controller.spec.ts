import { LoadService } from '../service/load/load.service';
import { Test, TestingModule } from '@nestjs/testing';
import { LoadController } from './load.controller';
jest.mock('../service/load/load.service');
jest.mock('../repository/mongoDB/process.repository');
describe('LoadControllerController', () => {
  let module: TestingModule, loadService: LoadService;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      controllers: [LoadController],
      providers: [LoadService],
    }).compile();
  });

  beforeEach(() => {
    loadService = module.get(LoadService);
  });

  it('should be defined', () => {
    const controller = module.get(LoadController);
    expect(controller).toBeDefined();
  });

  it('loadData() ', async () => {
    const ver = loadService.loadData();
    jest.spyOn(loadService, 'loadData').mockResolvedValue(ver);
    const controller = module.get(LoadController);
    expect(await controller.loadData()).toBe(ver);
  });

  it('loadDataCron() ', async () => {
    const ver = loadService.loadDataCron();
    jest.spyOn(loadService, 'loadData').mockResolvedValue(ver);
    const controller = module.get(LoadController);
    expect(await controller.loadData()).toBe(ver);
  });

});
