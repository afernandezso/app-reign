import {Controller, Get, InternalServerErrorException, Logger} from '@nestjs/common';
import { LoadService } from '../service/load/load.service';
import {ApiError} from "../error/api.error";

/**
 *  controller for api data loading
 */
@Controller('load')
export class LoadController {
  constructor(private readonly loadServiceService: LoadService) {
  }

  /**
   * method for loading api data
   */
  @Get('/')
  async loadData() {
    try {
    Logger.debug('LoadController loadData()');
    this.loadServiceService.loadData();
    } catch (error) {
      Logger.error('Error LoadController loadData()', error);
      new InternalServerErrorException(new ApiError("data could not be loaded, check log for more details"));
    }
  }
}
