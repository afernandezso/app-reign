import { Test, TestingModule } from '@nestjs/testing';
import { ProcessController } from './process.controller';
import { ProcessService } from '../service/process/process.service';
import { Process } from '../repository/mongoDB/entity/process.interface';
jest.mock('../service/process/process.service');

describe('ProcessController', () => {
  let module: TestingModule, service: ProcessService;
  let controller: ProcessController;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      controllers: [ProcessController],
      providers: [ProcessService],
    }).compile();
    service = module.get(ProcessService);
    controller = module.get(ProcessController);
  });

  it('should be defined', () => {
    const controller = module.get(ProcessController);
    expect(controller).toBeDefined();
  });

  it('getAll() expect empty', () => {
    let response: Process[];
    jest
      .spyOn(service, 'getAll')
      .mockImplementation((): Promise<Process[]> => Promise.resolve(response));
    expect(controller.getAll()).resolves.toEqual(response);
  });

  it('getAll() expect 1 length ', () => {
    const response: Process[] = [
      {
        objectID: '26995708',
        createdAt: '2021-04-30T16:36:10.000Z',
        title: 'Ask HN: Small SaaS, How much do you pay for hosting?',
        url: null,
        author: 'vhodges',
        state: true,
      },
    ];
    jest
      .spyOn(service, 'getAll')
      .mockImplementation((): Promise<Process[]> => Promise.resolve(response));
    expect(controller.getAll()).resolves.toEqual(response);
  });

  it('should delete', () => {
    const response: Process = {
      objectID: '26995708',
      createdAt: '2021-04-30T16:36:10.000Z',
      title: 'Ask HN: Small SaaS, How much do you pay for hosting?',
      url: null,
      author: 'vhodges',
      state: false,
    };
    jest
      .spyOn(service, 'delete')
      .mockImplementation((id: string) => Promise.resolve(response));
    expect(controller.delete('2')).resolves.toEqual(response);
  });

  it('should findById', () => {
    const response: Process = {
      objectID: '26995708',
      createdAt: '2021-04-30T16:36:10.000Z',
      title: 'Ask HN: Small SaaS, How much do you pay for hosting?',
      url: null,
      author: 'vhodges',
      state: true,
    };
    jest
      .spyOn(service, 'findById')
      .mockImplementation((id: string) => Promise.resolve(response));
    expect(controller.findById('2')).resolves.toEqual(response);
  });

  it('should deleteMany', () => {
    const response = {};
    jest
      .spyOn(service, 'deleteMany')
      .mockImplementation(() => Promise.resolve(response));
    expect(controller.deleteMany()).resolves.toEqual(response);
  });
});
