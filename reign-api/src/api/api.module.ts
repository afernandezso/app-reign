import { LoadService } from './service/load/load.service';
import { HttpCallsService } from './repository/http/httpCalls.service';
import { ProcessRepositoryService } from './repository/mongoDB/process.repository';

import { ProcessController } from './controller/process.controller';
import { ProcessService } from './service/process/process.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule, Module } from '@nestjs/common';
import { LoadController } from './controller/load.controller';
import { ProcessSchema } from './repository/mongoDB/entity/process.interface';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'data', schema: ProcessSchema }]),
  ],
  controllers: [LoadController, ProcessController],
  providers: [
    LoadService,
    HttpCallsService,
    ProcessRepositoryService,
    ProcessService,
  ],
})
export class ApiModule {}
