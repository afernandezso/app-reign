/**
 * class to map a hit to process
 */
export const LoadHitMapper = function (hits) {
  return hits.map(function (hit) {
    return {
      objectID: hit.objectID,
      createdAt: hit.created_at,
      title: hit.story_title || hit.title,
      url: hit.story_url || hit.url,
      author: hit.author,
      state: true,
    };
  });
};
