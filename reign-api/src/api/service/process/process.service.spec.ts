import { ProcessService } from './process.service';
import { TestingModule, Test } from '@nestjs/testing';
import { ProcessRepositoryService } from '../../repository/mongoDB/process.repository';
import { Process } from '../../repository/mongoDB/entity/process.interface';
jest.mock('../../repository/mongoDB/process.repository');
describe('ProcessService', () => {
  let service: ProcessService;
  let response: Process[];
  const process: Process = {
    objectID: '26995708',
    createdAt: '2021-04-30T16:36:10.000Z',
    title: 'Ask HN: Small SaaS, How much do you pay for hosting?',
    url: null,
    author: 'vhodges',
    state: true,
  };
  const mockProcessRepositoryService = {
    create: jest
      .fn()
      .mockImplementation((process: Process) => Promise.resolve(process)),
    getAll: jest
      .fn()
      .mockImplementation((): Promise<Process[]> => Promise.resolve(response)),
    findById: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve(process)),
    delete: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve(process)),
    deleteMany: jest.fn().mockImplementation(() => Promise.resolve({})),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProcessService,
        {
          provide: ProcessRepositoryService,
          useValue: mockProcessRepositoryService,
        },
      ],
    }).compile();

    service = module.get<ProcessService>(ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should getAll expect empty', () => {
    expect(service.getAll()).resolves.toEqual(response);
  });

  it('should findById', () => {
    expect(service.findById('1')).resolves.toEqual(process);
  });

  it('should delete', () => {
    expect(service.delete('1')).resolves.toEqual(process);
  });

  it('should deleteMany', () => {
    expect(service.deleteMany()).resolves.toEqual({});
  });
});
