import {
  Injectable,
  Logger,
  ServiceUnavailableException,
} from '@nestjs/common';
import { ProcessRepositoryService } from '../../repository/mongoDB/process.repository';
import { Process } from '../../repository/mongoDB/entity/process.interface';

/**
 * service to process the data
 */
@Injectable()
export class ProcessService {
  constructor(private processRepositoryService: ProcessRepositoryService) {}

  /**
   * method to get all data loaded
   */
  async getAll(): Promise<Process[]> {
    Logger.debug('ProcessService getAll()');
    return this.processRepositoryService.getAll();
  }

  /**
   * method to get a process by id
   */
  async findById(id: string): Promise<Process> {
    Logger.debug('ProcessService findById()')
    return this.processRepositoryService.findById(id);
  }

  /**
   * method to delete a process by id
   */
  async delete(id: string): Promise<Process> {
    Logger.debug('ProcessService delete()')
    return this.processRepositoryService.delete(id);
  }

  /**
   * method to delete all uploaded data uploaded data
   */
  async deleteMany() {
    Logger.debug('ProcessService deleteMany()')
    return this.processRepositoryService.deleteMany();
  }
}
