import { Test, TestingModule } from '@nestjs/testing';
import { LoadService } from './load.service';
import { ProcessRepositoryService } from '../../repository/mongoDB/process.repository';
import { getModelToken } from '@nestjs/mongoose';
import { HttpCallsService } from '../../repository/http/httpCalls.service';
import { of } from 'rxjs';
import { Process } from '../../repository/mongoDB/entity/process.interface';
jest.mock('../../repository/http/httpCalls.service');
jest.mock('../../repository/mongoDB/process.repository');
describe('LoadService', () => {
  const response = {
    hits: [
      {
        created_at: '2021-04-30T22:54:02.000Z',
        title: null,
        url: null,
        author: 'londons_explore',
        points: null,
        story_text: null,
        comment_text:
          'Isn\u0026#x27;t this the idea behind MIPMAPS in computer graphics?\u003cp\u003eIn the tracing world, I believe the opensource pulseview\u0026#x2F;sigrok does this.   It makes the UI very responsive even with gigabytes of data.    I just wish it also integrated data compression of some kind so I could fit more trace than I have RAM  (it can\u0026#x27;t be all that hard to intelligently compress a super repetitive signal in a way which still allows this fast zooming and scrolling - replacing some tree nodes with LZ77 style backreferences ought to do the trick)',
        num_comments: null,
        story_id: 27000110,
        story_title:
          'Implicit In-order Forests: Zooming a billion trace events at 60fps',
        story_url: 'https://thume.ca/2021/03/14/iforests/',
        parent_id: 27000110,
        created_at_i: 1619823242,
        _tags: ['comment', 'author_londons_explore', 'story_27000110'],
        objectID: '27000575',
        _highlightResult: {
          author: {
            value: 'londons_explore',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "Isn't this the idea behind MIPMAPS in computer graphics?\u003cp\u003eIn the tracing world, I believe the opensource pulseview/sigrok does this.   It makes the UI very responsive even with gigabytes of data.    I just wish it also integrated data compression of some kind so I could fit more trace than I have RAM  (it can't be all that hard to intelligently compress a super repetitive signal in a way which still allows this fast zooming and scrolling - replacing some tree \u003cem\u003enodes\u003c/em\u003e with LZ77 style backreferences ought to do the trick)",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Implicit In-order Forests: Zooming a billion trace events at 60fps',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://thume.ca/2021/03/14/iforests/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T21:25:01.000Z',
        title: null,
        url: null,
        author: 'mlex',
        points: null,
        story_text: null,
        comment_text:
          'Great article; in particular I hadn\u0026#x27;t thought about empty path components too closely and how websites usually just omit them when you try to go to e.g. \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;nodejs\u0026#x2F;\u0026#x2F;node" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;nodejs\u0026#x2F;\u0026#x2F;node\u003c/a\u003e',
        num_comments: null,
        story_id: 26991332,
        story_title: 'The Pains of Path Parsing',
        story_url: 'https://www.fpcomplete.com/blog/pains-path-parsing/',
        parent_id: 26991332,
        created_at_i: 1619817901,
        _tags: ['comment', 'author_mlex', 'story_26991332'],
        objectID: '26999555',
        _highlightResult: {
          author: { value: 'mlex', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'Great article; in particular I hadn\'t thought about empty path components too closely and how websites usually just omit them when you try to go to e.g. \u003ca href="https://github.com/nodejs//node" rel="nofollow"\u003ehttps://github.com/\u003cem\u003enodejs\u003c/em\u003e//node\u003c/a\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'The Pains of Path Parsing',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://www.fpcomplete.com/blog/pains-path-parsing/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T21:13:09.000Z',
        title: null,
        url: null,
        author: 'ignoramous',
        points: null,
        story_text: null,
        comment_text:
          'I used to read ThousandEyes\u0026#x27;s annual free reports on networking capabilities and measurements across the Big 3 cloud providers, which were quite comprehensive in terms of covering various scenarios [0]. I don\u0026#x27;t think they publish those anymore.\u003cp\u003eWith cloudping.co, it isn\u0026#x27;t clear from the website, but it seems like the inter-region latency they measure is over the public Internet. The Big 3 run their own backbone across all their DCs around the globe, and so I reckon, those numbers would look vastly different if traffic was instead relayed through those uncongested backbones.\u003cp\u003eWith AWS, the cheapest way I know (in terms of development time and cost) to accomplish inter-region over their backbone is via Global Load Accelerator. The clients connect to GLA at the nearest anycast IP location advertised across 150+ AWS PoPs. You can then play with endpoint-groups, connection-affinities, source\u0026#x2F;port tuples to control routing traffic to different backends in various regions.\u003cp\u003eWe used this technique to prototype a VPN with exit nodes in multiple countries but entry nodes closest to clients at every AWS PoP. It worked quite nicely for a toy: \u003ca href="https:\u0026#x2F;\u0026#x2F;news.ycombinator.com\u0026#x2F;item?id=21071593" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;news.ycombinator.com\u0026#x2F;item?id=21071593\u003c/a\u003e\u003cp\u003e[0] \u003ca href="https:\u0026#x2F;\u0026#x2F;www.thousandeyes.com\u0026#x2F;blog\u0026#x2F;top-takeaways-cloud-performance-benchmark" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.thousandeyes.com\u0026#x2F;blog\u0026#x2F;top-takeaways-cloud-perfor...\u003c/a\u003e (2019)',
        num_comments: null,
        story_id: 26996480,
        story_title: 'AWS inter-region latency chart',
        story_url: 'https://www.cloudping.co/grid',
        parent_id: 26996480,
        created_at_i: 1619817189,
        _tags: ['comment', 'author_ignoramous', 'story_26996480'],
        objectID: '26999423',
        _highlightResult: {
          author: { value: 'ignoramous', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'I used to read ThousandEyes\'s annual free reports on networking capabilities and measurements across the Big 3 cloud providers, which were quite comprehensive in terms of covering various scenarios [0]. I don\'t think they publish those anymore.\u003cp\u003eWith cloudping.co, it isn\'t clear from the website, but it seems like the inter-region latency they measure is over the public Internet. The Big 3 run their own backbone across all their DCs around the globe, and so I reckon, those numbers would look vastly different if traffic was instead relayed through those uncongested backbones.\u003cp\u003eWith AWS, the cheapest way I know (in terms of development time and cost) to accomplish inter-region over their backbone is via Global Load Accelerator. The clients connect to GLA at the nearest anycast IP location advertised across 150+ AWS PoPs. You can then play with endpoint-groups, connection-affinities, source/port tuples to control routing traffic to different backends in various regions.\u003cp\u003eWe used this technique to prototype a VPN with exit \u003cem\u003enodes\u003c/em\u003e in multiple countries but entry \u003cem\u003enodes\u003c/em\u003e closest to clients at every AWS PoP. It worked quite nicely for a toy: \u003ca href="https://news.ycombinator.com/item?id=21071593" rel="nofollow"\u003ehttps://news.ycombinator.com/item?id=21071593\u003c/a\u003e\u003cp\u003e[0] \u003ca href="https://www.thousandeyes.com/blog/top-takeaways-cloud-performance-benchmark" rel="nofollow"\u003ehttps://www.thousandeyes.com/blog/top-takeaways-cloud-perfor...\u003c/a\u003e (2019)',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'AWS inter-region latency chart',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://www.cloudping.co/grid',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T20:49:14.000Z',
        title: null,
        url: null,
        author: 'guardiangod',
        points: null,
        story_text: null,
        comment_text:
          '\u0026gt; We build that fab equipment in Europe.\u003cp\u003eEurope builds _1_ component for one fab stage (the EUV litho machines). For the other stages, there are other manufacturers in other countries.\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;archive.ph\u0026#x2F;HEI2z" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;archive.ph\u0026#x2F;HEI2z\u003c/a\u003e\u003cp\u003eJapan is still the dominate supplier of the chemicals used in silicon fabs.\u003cp\u003eThe majority of the process is still DUV layers (less advance than EUV), and Japan\u0026#x27;s Nikon\u0026#x2F;Canon are the leaders in DUV.\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;seekingalpha.com\u0026#x2F;article\u0026#x2F;4412366-asml-not-dominant-without-euv" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;seekingalpha.com\u0026#x2F;article\u0026#x2F;4412366-asml-not-dominant-w...\u003c/a\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  Secondly, TSMC and Samsung use EUV for only a few chip layers, and use DUV immersion for the rest, so both types of systems are needed at and below 7nm. The number of mask layers increases as nodes decrease.\n\n    A 28nm IC has up to 50 layers, a 14nm\u0026#x2F;10nm has 60 layers,\n\n    A 7nm IC has 80 layers, and\n\n    A 5nm IC has 100, depending on manufacturer.\n\n   TSMC uses EUV for just 12 layers at 7nm vs. 68 DUV layers. TSMC uses EUV for 22 layers at 5nm vs 78 DUV layers. Immersion DUV is critical to make these chips, and ASML is losing share to Nikon.\u003c/code\u003e\u003c/pre\u003e',
        num_comments: null,
        story_id: 26993345,
        story_title: 'Intel seeks $10B in subsidies for European chip plant',
        story_url:
          'https://www.reuters.com/technology/intel-seeks-8-bln-euros-subsidies-european-chip-plant-politico-2021-04-30/',
        parent_id: 26997782,
        created_at_i: 1619815754,
        _tags: ['comment', 'author_guardiangod', 'story_26993345'],
        objectID: '26999117',
        _highlightResult: {
          author: {
            value: 'guardiangod',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              '\u0026gt; We build that fab equipment in Europe.\u003cp\u003eEurope builds _1_ component for one fab stage (the EUV litho machines). For the other stages, there are other manufacturers in other countries.\u003cp\u003e\u003ca href="https://archive.ph/HEI2z" rel="nofollow"\u003ehttps://archive.ph/HEI2z\u003c/a\u003e\u003cp\u003eJapan is still the dominate supplier of the chemicals used in silicon fabs.\u003cp\u003eThe majority of the process is still DUV layers (less advance than EUV), and Japan\'s Nikon/Canon are the leaders in DUV.\u003cp\u003e\u003ca href="https://seekingalpha.com/article/4412366-asml-not-dominant-without-euv" rel="nofollow"\u003ehttps://seekingalpha.com/article/4412366-asml-not-dominant-w...\u003c/a\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  Secondly, TSMC and Samsung use EUV for only a few chip layers, and use DUV immersion for the rest, so both types of systems are needed at and below 7nm. The number of mask layers increases as \u003cem\u003enodes\u003c/em\u003e decrease.\n\n    A 28nm IC has up to 50 layers, a 14nm/10nm has 60 layers,\n\n    A 7nm IC has 80 layers, and\n\n    A 5nm IC has 100, depending on manufacturer.\n\n   TSMC uses EUV for just 12 layers at 7nm vs. 68 DUV layers. TSMC uses EUV for 22 layers at 5nm vs 78 DUV layers. Immersion DUV is critical to make these chips, and ASML is losing share to Nikon.\u003c/code\u003e\u003c/pre\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Intel seeks $10B in subsidies for European chip plant',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.reuters.com/technology/intel-seeks-8-bln-euros-subsidies-european-chip-plant-politico-2021-04-30/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T20:45:51.000Z',
        title: null,
        url: null,
        author: 'ineedasername',
        points: null,
        story_text: null,
        comment_text:
          'Trust approaches, but never reaches, zero. In practice though, so few people run full nodes relative to the whole that trust is still not nearly as decentralized as it might appear. A few rough estimates from simple searches indicates that only 10% of about 1 million miners actually run a full node.\u003cp\u003eFurther, while this article is about 2 years old, it indicates that many miners use outdated software that may have vulnerabilities. [0] Even assuming those expressed in the article have resolved, it may very well be the case that similar proportions of miners today aren\u0026#x27;t running the most recent software \u0026amp; are vulnerable to newer attacks.\u003cp\u003eFinally, while bitcoin may have ossified, I don\u0026#x27;t think that is has done so as a store of value (at least not year). A solid store of value should not fluctuate in value by 5%, 10%, 15% on a fairly regular basis.\u003cp\u003eIf I wanted a good store of value, I would still be looking at the traditional option of gold or, at least over the long-term, real estate. Though I suppose if the world economic system every goes belly up, neither bitcoin, gold, or real estate will be of much use. In that case, the best store of value would be long-term shelf-stable food.\u003cp\u003e[0] \u003ca href="https:\u0026#x2F;\u0026#x2F;thenextweb.com\u0026#x2F;news\u0026#x2F;bitcoin-100000-nodes-vulnerable-cryptocurrency" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;thenextweb.com\u0026#x2F;news\u0026#x2F;bitcoin-100000-nodes-vulnerable-...\u003c/a\u003e',
        num_comments: null,
        story_id: 26993868,
        story_title: "'Minting' electronic cash (1999)",
        story_url:
          'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
        parent_id: 26997743,
        created_at_i: 1619815551,
        _tags: ['comment', 'author_ineedasername', 'story_26993868'],
        objectID: '26999074',
        _highlightResult: {
          author: {
            value: 'ineedasername',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'Trust approaches, but never reaches, zero. In practice though, so few people run full \u003cem\u003enodes\u003c/em\u003e relative to the whole that trust is still not nearly as decentralized as it might appear. A few rough estimates from simple searches indicates that only 10% of about 1 million miners actually run a full node.\u003cp\u003eFurther, while this article is about 2 years old, it indicates that many miners use outdated software that may have vulnerabilities. [0] Even assuming those expressed in the article have resolved, it may very well be the case that similar proportions of miners today aren\'t running the most recent software \u0026amp; are vulnerable to newer attacks.\u003cp\u003eFinally, while bitcoin may have ossified, I don\'t think that is has done so as a store of value (at least not year). A solid store of value should not fluctuate in value by 5%, 10%, 15% on a fairly regular basis.\u003cp\u003eIf I wanted a good store of value, I would still be looking at the traditional option of gold or, at least over the long-term, real estate. Though I suppose if the world economic system every goes belly up, neither bitcoin, gold, or real estate will be of much use. In that case, the best store of value would be long-term shelf-stable food.\u003cp\u003e[0] \u003ca href="https://thenextweb.com/news/bitcoin-100000-nodes-vulnerable-cryptocurrency" rel="nofollow"\u003ehttps://thenextweb.com/news/bitcoin-100000-\u003cem\u003enodes\u003c/em\u003e-vulnerable-...\u003c/a\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: "'Minting' electronic cash (1999)",
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T19:38:56.000Z',
        title: null,
        url: null,
        author: '1e-9',
        points: null,
        story_text: null,
        comment_text:
          '\u0026gt; As trust is spread across the network to an increasing number of people running nodes, the trust assigned to any individual participant approaches zero.\u003cp\u003eIf only we knew this was true.\u003cp\u003eAn increase in the number of people running nodes does not guarantee that the maximum trust assigned to any individual participant approaches zero.  What is the size of the largest Bitcoin whale?  What is the size of the largest Bitcoin cartel?  How do you know that the share of the largest Bitcoin whale or cartel isn’t increasing?  What prevents China from deciding to eliminate the Bitcoin threat to its control by taking over the Chinese miners and launching a 51% attack?\u003cp\u003eEven if you are willing to believe that trust is well distributed across many independent entities, you still need to have greater trust in your own security and IT skills.  When your wallet is hacked or you lose your credentials, there’s no number to call to recover your Bitcoin.\u003cp\u003eBitcoin still requires significant trust, which is often overlooked.',
        num_comments: null,
        story_id: 26993868,
        story_title: "'Minting' electronic cash (1999)",
        story_url:
          'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
        parent_id: 26997743,
        created_at_i: 1619811536,
        _tags: ['comment', 'author_1e-9', 'story_26993868'],
        objectID: '26998267',
        _highlightResult: {
          author: { value: '1e-9', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              '\u0026gt; As trust is spread across the network to an increasing number of people running \u003cem\u003enodes\u003c/em\u003e, the trust assigned to any individual participant approaches zero.\u003cp\u003eIf only we knew this was true.\u003cp\u003eAn increase in the number of people running \u003cem\u003enodes\u003c/em\u003e does not guarantee that the maximum trust assigned to any individual participant approaches zero.  What is the size of the largest Bitcoin whale?  What is the size of the largest Bitcoin cartel?  How do you know that the share of the largest Bitcoin whale or cartel isn’t increasing?  What prevents China from deciding to eliminate the Bitcoin threat to its control by taking over the Chinese miners and launching a 51% attack?\u003cp\u003eEven if you are willing to believe that trust is well distributed across many independent entities, you still need to have greater trust in your own security and IT skills.  When your wallet is hacked or you lose your credentials, there’s no number to call to recover your Bitcoin.\u003cp\u003eBitcoin still requires significant trust, which is often overlooked.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: "'Minting' electronic cash (1999)",
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T19:09:33.000Z',
        title: null,
        url: null,
        author: 'thu2111',
        points: null,
        story_text: null,
        comment_text:
          'Yes but the only ones not on long since obsolete nodes are the Intel fabs in Ireland, I think.',
        num_comments: null,
        story_id: 26993345,
        story_title: 'Intel seeks $10 bln in subsidies for European chip plant',
        story_url:
          'https://www.reuters.com/technology/intel-seeks-8-bln-euros-subsidies-european-chip-plant-politico-2021-04-30/',
        parent_id: 26997580,
        created_at_i: 1619809773,
        _tags: ['comment', 'author_thu2111', 'story_26993345'],
        objectID: '26997901',
        _highlightResult: {
          author: { value: 'thu2111', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'Yes but the only ones not on long since obsolete \u003cem\u003enodes\u003c/em\u003e are the Intel fabs in Ireland, I think.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Intel seeks $10 bln in subsidies for European chip plant',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.reuters.com/technology/intel-seeks-8-bln-euros-subsidies-european-chip-plant-politico-2021-04-30/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T18:56:53.000Z',
        title: null,
        url: null,
        author: 'rogueSkib',
        points: null,
        story_text: null,
        comment_text:
          'I don\u0026#x27;t believe this is accurate.\u003cp\u003eAs trust is spread across the network to an increasing number of people running nodes, the trust assigned to any individual participant approaches zero.\u003cp\u003eThis can be observed in the resilience of the network, as it self-heals against any attacker or alternative fork from the consensus.\u003cp\u003eBitcoin has democratically ossified into a store-of-value with absolute scarcity, deterministic monetary policy, and a priority on decentralization. It\u0026#x27;s extremely unlikely that those attributes will be compromised.',
        num_comments: null,
        story_id: 26993868,
        story_title: "'Minting' electronic cash (1999)",
        story_url:
          'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
        parent_id: 26997283,
        created_at_i: 1619809013,
        _tags: ['comment', 'author_rogueSkib', 'story_26993868'],
        objectID: '26997743',
        _highlightResult: {
          author: { value: 'rogueSkib', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "I don't believe this is accurate.\u003cp\u003eAs trust is spread across the network to an increasing number of people running \u003cem\u003enodes\u003c/em\u003e, the trust assigned to any individual participant approaches zero.\u003cp\u003eThis can be observed in the resilience of the network, as it self-heals against any attacker or alternative fork from the consensus.\u003cp\u003eBitcoin has democratically ossified into a store-of-value with absolute scarcity, deterministic monetary policy, and a priority on decentralization. It's extremely unlikely that those attributes will be compromised.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: "'Minting' electronic cash (1999)",
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://spectrum.ieee.org/computing/software/minting-electronic-cash',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T16:41:57.000Z',
        title: null,
        url: null,
        author: 'nodesocket',
        points: null,
        story_text: null,
        comment_text:
          '\u0026gt; give the police temporary access to track it, til they catch the thief and get my stuff back.\u003cp\u003eI don’t know where you live, but as somebody who previously lived in San Francisco, do you really think the police are going to actively pursue theft claims? It’s why car break-ins and petty theft are out of control in the bay area. The police don’t enforce laws, and certainly don’t actively track down stolen property.',
        num_comments: null,
        story_id: 26993755,
        story_title:
          'Apple reveals more about AirTag stalking protections as domestic abuse concerns',
        story_url:
          'https://9to5mac.com/2021/04/30/airtag-stalking-protections/',
        parent_id: 26995353,
        created_at_i: 1619800917,
        _tags: ['comment', 'author_nodesocket', 'story_26993755'],
        objectID: '26995794',
        _highlightResult: {
          author: {
            value: '\u003cem\u003enodes\u003c/em\u003eocket',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          comment_text: {
            value:
              '\u0026gt; give the police temporary access to track it, til they catch the thief and get my stuff back.\u003cp\u003eI don’t know where you live, but as somebody who previously lived in San Francisco, do you really think the police are going to actively pursue theft claims? It’s why car break-ins and petty theft are out of control in the bay area. The police don’t enforce laws, and certainly don’t actively track down stolen property.',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_title: {
            value:
              'Apple reveals more about AirTag stalking protections as domestic abuse concerns',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://9to5mac.com/2021/04/30/airtag-stalking-protections/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T16:36:10.000Z',
        title: null,
        url: null,
        author: 'vhodges',
        points: null,
        story_text: null,
        comment_text:
          'I have a small SaaS app running on a VPS at Linode.  It\u0026#x27;s costing 8USD\u0026#x2F;Month:\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  * $5\u0026#x2F;Month for a 1GB Ram Micro (Postgresql single instance, Rails)\n  * $2\u0026#x2F;Month for Backups \n  * $1\u0026#x2F;Month for block storage (10GB)\n\u003c/code\u003e\u003c/pre\u003e\nThat being said, I am planning a move to fly.io to get a replicated HA Postgress and my rails app.  I expect it to run mostly on their free tier plus a bit for more than the things not covered on the free tier for a total of about $8.70\u0026#x2F;Month:\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  * $3\u0026#x2F;Month for 2x10GB block storage (1 each for the postgres nodes)\n  * $5.70\u0026#x2F;Month Shared CPU, 1 GB Ram VM (For the Rails app)\n\u003c/code\u003e\u003c/pre\u003e\nSo a wash price wise.  But fly.io will get me a better CI\u0026#x2F;CD\u0026#x2F;Deployment story, replicated HA Db, easy scale the db and app across geographic regions and a easy way to scale period.\u003cp\u003eRight now I\u0026#x27;d need to build another box and get it hooked up, figure out a LB for it (or pay for Linodes), etc. I CAN automate some of that (Terraform, et al), but there\u0026#x27;s a learning curve and implementation effort cost.  All stuff I am perfectly capable of doing but as a solo founder, I would rather spend time building value (features) and work on marketing \u0026amp; sales.\u003cp\u003eHosted K8s at Linode or DO were options too, but still learning curve and the cost (to get started) would have been 5-10 times higher for the same thing (and I\u0026#x27;m cheap :).  Time will tell the cost curve as I scale, but it\u0026#x27;s a small app with a limited audience  so it doesn\u0026#x27;t need much.',
        num_comments: null,
        story_id: 26994915,
        story_title: 'Ask HN: Small SaaS, How much do you pay for hosting?',
        story_url: null,
        parent_id: 26994915,
        created_at_i: 1619800570,
        _tags: ['comment', 'author_vhodges', 'story_26994915'],
        objectID: '26995708',
        _highlightResult: {
          author: { value: 'vhodges', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "I have a small SaaS app running on a VPS at Linode.  It's costing 8USD/Month:\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  * $5/Month for a 1GB Ram Micro (Postgresql single instance, Rails)\n  * $2/Month for Backups \n  * $1/Month for block storage (10GB)\n\u003c/code\u003e\u003c/pre\u003e\nThat being said, I am planning a move to fly.io to get a replicated HA Postgress and my rails app.  I expect it to run mostly on their free tier plus a bit for more than the things not covered on the free tier for a total of about $8.70/Month:\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  * $3/Month for 2x10GB block storage (1 each for the postgres \u003cem\u003enodes\u003c/em\u003e)\n  * $5.70/Month Shared CPU, 1 GB Ram VM (For the Rails app)\n\u003c/code\u003e\u003c/pre\u003e\nSo a wash price wise.  But fly.io will get me a better CI/CD/Deployment story, replicated HA Db, easy scale the db and app across geographic regions and a easy way to scale period.\u003cp\u003eRight now I'd need to build another box and get it hooked up, figure out a LB for it (or pay for Linodes), etc. I CAN automate some of that (Terraform, et al), but there's a learning curve and implementation effort cost.  All stuff I am perfectly capable of doing but as a solo founder, I would rather spend time building value (features) and work on marketing \u0026amp; sales.\u003cp\u003eHosted K8s at Linode or DO were options too, but still learning curve and the cost (to get started) would have been 5-10 times higher for the same thing (and I'm cheap :).  Time will tell the cost curve as I scale, but it's a small app with a limited audience  so it doesn't need much.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Ask HN: Small SaaS, How much do you pay for hosting?',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T15:36:36.000Z',
        title: 'Modern JavaScript Explained for Dinosaurs',
        url:
          'https://medium.com/the-node-js-collection/modern-javascript-explained-for-dinosaurs-f695e9747b70',
        author: 'mikecarlton',
        points: 1,
        story_text: null,
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1619796996,
        _tags: ['story', 'author_mikecarlton', 'story_26994823'],
        objectID: '26994823',
        _highlightResult: {
          title: {
            value: 'Modern JavaScript Explained for Dinosaurs',
            matchLevel: 'none',
            matchedWords: [],
          },
          url: {
            value:
              'https://medium.com/the-\u003cem\u003enode-js\u003c/em\u003e-collection/modern-javascript-explained-for-dinosaurs-f695e9747b70',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          author: {
            value: 'mikecarlton',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T15:28:41.000Z',
        title: 'Today, Node.js 10 is going EOL',
        url: 'https://twitter.com/nodejs/status/1388116425361874945',
        author: 'themarkers',
        points: 6,
        story_text: null,
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1619796521,
        _tags: ['story', 'author_themarkers', 'story_26994698'],
        objectID: '26994698',
        _highlightResult: {
          title: {
            value: 'Today, Node.js 10 is going EOL',
            matchLevel: 'none',
            matchedWords: [],
          },
          url: {
            value:
              'https://twitter.com/\u003cem\u003enodejs\u003c/em\u003e/status/1388116425361874945',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          author: { value: 'themarkers', matchLevel: 'none', matchedWords: [] },
        },
      },
      {
        created_at: '2021-04-30T14:46:59.000Z',
        title: null,
        url: null,
        author: 'd1plo1d',
        points: null,
        story_text: null,
        comment_text:
          'For reference I have over a decade of JavaScript experience in industry and my async Rust rewrite of a large JS project was *more* concise then the heavily refactored and polished NodeJS version (a language I consider more concise then most). If you are having to copy and paste excessively in Rust that is an issue but it is not necessarily intrinsic to the language.\u003cp\u003eFor what it\u0026#x27;s worth traits largely prevented copy and paste and where traits fail there are macros. The classic inheritance example you link to is a tiny percentage of my code and an orders of magnitude smaller time sink when compared to the code maintenance problems I faced in other languages.',
        num_comments: null,
        story_id: 26988839,
        story_title: 'Inheritance was invented as a performance hack',
        story_url: 'http://catern.com/inheritance.html',
        parent_id: 26992001,
        created_at_i: 1619794019,
        _tags: ['comment', 'author_d1plo1d', 'story_26988839'],
        objectID: '26994120',
        _highlightResult: {
          author: { value: 'd1plo1d', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "For reference I have over a decade of JavaScript experience in industry and my async Rust rewrite of a large JS project was *more* concise then the heavily refactored and polished \u003cem\u003eNodeJS\u003c/em\u003e version (a language I consider more concise then most). If you are having to copy and paste excessively in Rust that is an issue but it is not necessarily intrinsic to the language.\u003cp\u003eFor what it's worth traits largely prevented copy and paste and where traits fail there are macros. The classic inheritance example you link to is a tiny percentage of my code and an orders of magnitude smaller time sink when compared to the code maintenance problems I faced in other languages.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Inheritance was invented as a performance hack',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'http://catern.com/inheritance.html',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T13:29:10.000Z',
        title: null,
        url: null,
        author: 'mfer',
        points: null,
        story_text: null,
        comment_text:
          'First, this is about more than codecs and quality. The article notes that Google wants data on users that other channels don\u0026#x27;t get.\u003cp\u003eHardware may be part of it, though.\u003cp\u003eCodecs also affect bandwidth. Videos in newer codecs often have a smaller file size which is less bandwidth to stream and smaller files to keep on their edge nodes. At the scale of Google and Netflix this is substantial and plays into their costs.\u003cp\u003eGoogle, via YouTube, has a history of only wanting to support the latest systems. I\u0026#x27;ve had older devices where the YouTube app has gone away. This is a bit of forced obsolescence to push people to buy newer stuff. Not because they need it but for reasons outside the end users needs.',
        num_comments: null,
        story_id: 26992358,
        story_title:
          'YouTube TV removed from Roku channel store amid Google contract dispute',
        story_url:
          'https://www.axios.com/roku-youtube-tv-removed-google-5580ffdf-b865-428c-91ca-8ad83802fedf.html',
        parent_id: 26992738,
        created_at_i: 1619789350,
        _tags: ['comment', 'author_mfer', 'story_26992358'],
        objectID: '26993182',
        _highlightResult: {
          author: { value: 'mfer', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "First, this is about more than codecs and quality. The article notes that Google wants data on users that other channels don't get.\u003cp\u003eHardware may be part of it, though.\u003cp\u003eCodecs also affect bandwidth. Videos in newer codecs often have a smaller file size which is less bandwidth to stream and smaller files to keep on their edge \u003cem\u003enodes\u003c/em\u003e. At the scale of Google and Netflix this is substantial and plays into their costs.\u003cp\u003eGoogle, via YouTube, has a history of only wanting to support the latest systems. I've had older devices where the YouTube app has gone away. This is a bit of forced obsolescence to push people to buy newer stuff. Not because they need it but for reasons outside the end users needs.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'YouTube TV removed from Roku channel store amid Google contract dispute',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.axios.com/roku-youtube-tv-removed-google-5580ffdf-b865-428c-91ca-8ad83802fedf.html',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T13:18:25.000Z',
        title: null,
        url: null,
        author: 'saurik',
        points: null,
        story_text: null,
        comment_text:
          'The usual implementation is you simply check every few minutes which nodes are up and then in your A response you return all the working addresses.... are you trying to do something more complex?',
        num_comments: null,
        story_id: 26987939,
        story_title: 'We Built Our Own DNS Infrastructure',
        story_url: 'https://blog.replit.com/dns',
        parent_id: 26992310,
        created_at_i: 1619788705,
        _tags: ['comment', 'author_saurik', 'story_26987939'],
        objectID: '26993028',
        _highlightResult: {
          author: { value: 'saurik', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'The usual implementation is you simply check every few minutes which \u003cem\u003enodes\u003c/em\u003e are up and then in your A response you return all the working addresses.... are you trying to do something more complex?',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'We Built Our Own DNS Infrastructure',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://blog.replit.com/dns',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T12:04:11.000Z',
        title: null,
        url: null,
        author: 'xxs',
        points: null,
        story_text: null,
        comment_text:
          'Obviously you can do\u003cp\u003epublic class Node\u0026lt;T\u0026gt;{ \n    Node prev, next;\n\u0026#x2F;\u0026#x2F;stuff comes here\n....\n  }\u003cp\u003eThen extend and have the overall code that deals with modifying the datastructure, but it\u0026#x27;s overall ugly. Personally I have written enough datastructures where linking nodes is useful. For example: red\u0026#x2F;black tree + insert order \u0026#x27;next\u0026#x27; makes for a decent implementation of a moving median. \nYet, that\u0026#x27;s not what almost any developer would do normally.',
        num_comments: null,
        story_id: 26988839,
        story_title: 'Inheritance was invented as a performance hack',
        story_url: 'http://catern.com/inheritance.html',
        parent_id: 26991671,
        created_at_i: 1619784251,
        _tags: ['comment', 'author_xxs', 'story_26988839'],
        objectID: '26992337',
        _highlightResult: {
          author: { value: 'xxs', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "Obviously you can do\u003cp\u003epublic class Node\u0026lt;T\u0026gt;{ \n    Node prev, next;\n//stuff comes here\n....\n  }\u003cp\u003eThen extend and have the overall code that deals with modifying the datastructure, but it's overall ugly. Personally I have written enough datastructures where linking \u003cem\u003enodes\u003c/em\u003e is useful. For example: red/black tree + insert order 'next' makes for a decent implementation of a moving median. \nYet, that's not what almost any developer would do normally.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Inheritance was invented as a performance hack',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'http://catern.com/inheritance.html',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T07:34:29.000Z',
        title: null,
        url: null,
        author: 'gallexme',
        points: null,
        story_text: null,
        comment_text:
          '\u0026gt; I\u0026#x27;m pretty unfamiliar with BEAM. Does this \u0026quot;processes all the way down\u0026quot; span across machines\u0026#x2F;VMs?\u003cp\u003eyes for example if u had a process named on a different Machine(Node called in Erlang) called \u0026quot;Alice\u0026quot;, u could from a different Node send it a message using the Node Identifier as additional parameter example:\u003cp\u003e[coolest_node | _rest_of_nodes] = Node.list()\u003cp\u003eProcess.send({Alice, coolest_node }, :hi)',
        num_comments: null,
        story_id: 26989577,
        story_title: 'Building a Distributed Turn-Based Game System in Elixir',
        story_url:
          'https://fly.io/blog/building-a-distributed-turn-based-game-system-in-elixir/',
        parent_id: 26990635,
        created_at_i: 1619768069,
        _tags: ['comment', 'author_gallexme', 'story_26989577'],
        objectID: '26990728',
        _highlightResult: {
          author: { value: 'gallexme', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "\u0026gt; I'm pretty unfamiliar with BEAM. Does this \u0026quot;processes all the way down\u0026quot; span across machines/VMs?\u003cp\u003eyes for example if u had a process named on a different Machine(Node called in Erlang) called \u0026quot;Alice\u0026quot;, u could from a different Node send it a message using the Node Identifier as additional parameter example:\u003cp\u003e[coolest_node | _rest_of_\u003cem\u003enodes\u003c/em\u003e] = Node.list()\u003cp\u003eProcess.send({Alice, coolest_node }, :hi)",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Building a Distributed Turn-Based Game System in Elixir',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://fly.io/blog/building-a-distributed-turn-based-game-system-in-elixir/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T07:20:02.000Z',
        title: null,
        url: null,
        author: 'tomgp',
        points: null,
        story_text: null,
        comment_text:
          'I’ve been thinking of trying out elixir for exactly this kind of thing. Having done similar projects in Nodejs (experience was fine tbh) and used Xstate or Redux for game state management on the server side. What would people recommend for that role in Elixir (or is it a case of roll yr own)',
        num_comments: null,
        story_id: 26989577,
        story_title: 'Building a Distributed Turn-Based Game System in Elixir',
        story_url:
          'https://fly.io/blog/building-a-distributed-turn-based-game-system-in-elixir/',
        parent_id: 26989577,
        created_at_i: 1619767202,
        _tags: ['comment', 'author_tomgp', 'story_26989577'],
        objectID: '26990662',
        _highlightResult: {
          author: { value: 'tomgp', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'I’ve been thinking of trying out elixir for exactly this kind of thing. Having done similar projects in \u003cem\u003eNodejs\u003c/em\u003e (experience was fine tbh) and used Xstate or Redux for game state management on the server side. What would people recommend for that role in Elixir (or is it a case of roll yr own)',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Building a Distributed Turn-Based Game System in Elixir',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://fly.io/blog/building-a-distributed-turn-based-game-system-in-elixir/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T06:34:01.000Z',
        title: null,
        url: null,
        author: 'benrbray',
        points: null,
        story_text: null,
        comment_text:
          'I use roamlikes as a combined bookmark \u0026#x2F; note-taking system.\u003cp\u003eI tend to spend a few minutes every day taking some interesting links from HN or elsewhere (that I may or may not read that day) and creating entries for them in my personal wiki system.\u003cp\u003e(This is how I keep track of collections of related links -- for my post above, I just searched for [[knowledge-graph]] tags in my wiki system and copied the results into an HN comment)\u003cp\u003eI find this most useful for topics that I know I\u0026#x27;d like to explore in the future, but just don\u0026#x27;t have time for right now.  The Google search signal-to-noise ratio has lessened so much that I know I\u0026#x27;ll never be able to find a specific link again unless I remember the exact title, so it\u0026#x27;s useful to have a way to quickly assign tags to a URL and forget about it until later.  Tagging systems are much better than a hierarchical bookmark system for recall later [1].\u003cp\u003eThen, when I take the time to read about a certain topic, I start taking notes in the .md file corresponding to the appropriate keyword or citation.\u003cp\u003eThe best part is discovering new connections between topics -- sometimes I\u0026#x27;ll type a [[keyword]] in my notes, and see that several of my existing notes already link to it.\u003cp\u003eI\u0026#x27;m still learning how to best use a system like this, it does take some effort to maintain.  Right now what works for me is really short, concise notes about very specific keywords to start.  Then I\u0026#x27;ll do a \u0026quot;synthesis\u0026quot; pass where I\u0026#x27;ll summarize the relationships between several keywords all in one document, and then make all the keyword nodes in the graph point to the synthesized document instead.\u003cp\u003e[1] Nayuki, \u0026quot;Designing Better File Organization around Tags, Not Hierarchies\u0026quot; -- \u003ca href="https:\u0026#x2F;\u0026#x2F;www.nayuki.io\u0026#x2F;page\u0026#x2F;designing-better-file-organization-around-tags-not-hierarchies" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.nayuki.io\u0026#x2F;page\u0026#x2F;designing-better-file-organizatio...\u003c/a\u003e',
        num_comments: null,
        story_id: 26981864,
        story_title:
          'Cognicull: Knowledge base for mathematics, natural science and engineering',
        story_url: 'https://cognicull.com/',
        parent_id: 26990141,
        created_at_i: 1619764441,
        _tags: ['comment', 'author_benrbray', 'story_26981864'],
        objectID: '26990417',
        _highlightResult: {
          author: { value: 'benrbray', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "I use roamlikes as a combined bookmark / note-taking system.\u003cp\u003eI tend to spend a few minutes every day taking some interesting links from HN or elsewhere (that I may or may not read that day) and creating entries for them in my personal wiki system.\u003cp\u003e(This is how I keep track of collections of related links -- for my post above, I just searched for [[knowledge-graph]] tags in my wiki system and copied the results into an HN comment)\u003cp\u003eI find this most useful for topics that I know I'd like to explore in the future, but just don't have time for right now.  The Google search signal-to-noise ratio has lessened so much that I know I'll never be able to find a specific link again unless I remember the exact title, so it's useful to have a way to quickly assign tags to a URL and forget about it until later.  Tagging systems are much better than a hierarchical bookmark system for recall later [1].\u003cp\u003eThen, when I take the time to read about a certain topic, I start taking notes in the .md file corresponding to the appropriate keyword or citation.\u003cp\u003eThe best part is discovering new connections between topics -- sometimes I'll type a [[keyword]] in my notes, and see that several of my existing notes already link to it.\u003cp\u003eI'm still learning how to best use a system like this, it does take some effort to maintain.  Right now what works for me is really short, concise notes about very specific keywords to start.  Then I'll do a \u0026quot;synthesis\u0026quot; pass where I'll summarize the relationships between several keywords all in one document, and then make all the keyword \u003cem\u003enodes\u003c/em\u003e in the graph point to the synthesized document instead.\u003cp\u003e[1] Nayuki, \u0026quot;Designing Better File Organization around Tags, Not Hierarchies\u0026quot; -- \u003ca href=\"https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies\" rel=\"nofollow\"\u003ehttps://www.nayuki.io/page/designing-better-file-organizatio...\u003c/a\u003e",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Cognicull: Knowledge base for mathematics, natural science and engineering',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://cognicull.com/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2021-04-30T03:16:32.000Z',
        title: null,
        url: null,
        author: 'benbjohnson',
        points: null,
        story_text: null,
        comment_text:
          'Litestream flushes out changes to S3 on an interval. By default, it’s every 10 seconds although you could reasonably drop that down to 1 second. You could go lower if you’re replicating to NFS.\u003cp\u003eThat interval determines your window for data loss in the event of a catastrophic failure (e.g. disk failure). I’ve run multiple DigitalOcean machines for years and have never had a catastrophic failure so they are rare events but they can happen.\u003cp\u003eDqlite and rqlite run every write through a distributed consensus algorithm which guarantees that your writes are persisted across at least a majority of nodes in your cluster before returning a success. In that situation, you’d need a catastrophic failure on a majority of nodes to lose data, however, there is generally a large performance trade off when running distributed consensus.',
        num_comments: null,
        story_id: 26981239,
        story_title: 'Litestream Eliminated My Database Server for $0.03/Month',
        story_url: 'https://mtlynch.io/litestream/',
        parent_id: 26987228,
        created_at_i: 1619752592,
        _tags: ['comment', 'author_benbjohnson', 'story_26981239'],
        objectID: '26989540',
        _highlightResult: {
          author: {
            value: 'benbjohnson',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'Litestream flushes out changes to S3 on an interval. By default, it’s every 10 seconds although you could reasonably drop that down to 1 second. You could go lower if you’re replicating to NFS.\u003cp\u003eThat interval determines your window for data loss in the event of a catastrophic failure (e.g. disk failure). I’ve run multiple DigitalOcean machines for years and have never had a catastrophic failure so they are rare events but they can happen.\u003cp\u003eDqlite and rqlite run every write through a distributed consensus algorithm which guarantees that your writes are persisted across at least a majority of \u003cem\u003enodes\u003c/em\u003e in your cluster before returning a success. In that situation, you’d need a catastrophic failure on a majority of \u003cem\u003enodes\u003c/em\u003e to lose data, however, there is generally a large performance trade off when running distributed consensus.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Litestream Eliminated My Database Server for $0.03/Month',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://mtlynch.io/litestream/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
    ],
    nbHits: 20268,
    page: 0,
    nbPages: 50,
    hitsPerPage: 20,
    exhaustiveNbHits: true,
    query: 'nodejs',
    params:
      'advancedSyntax=true\u0026analytics=true\u0026analyticsTags=backend\u0026query=nodejs',
    processingTimeMS: 7,
    status: 1,
    headers: '',
    statusText: 'ok',
    config: {},
  };

  let service: LoadService;

  const mockHttpCallsService = {
    getDataAlgolia: jest.fn().mockImplementation(() => of(response)),
  };
  const mockProcessRepositoryService = {
    create: jest
      .fn()
      .mockImplementation((process: Process) => Promise.resolve(process)),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LoadService,
        { provide: HttpCallsService, useValue: mockHttpCallsService },
        {
          provide: ProcessRepositoryService,
          useValue: mockProcessRepositoryService,
        },
      ],
    }).compile();

    service = module.get<LoadService>(LoadService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be definedd', () => {
    let process: Process;
    expect(service.loadData()).toBeDefined();
    expect(mockProcessRepositoryService.create).toHaveBeenCalledTimes(0);
  });
});
