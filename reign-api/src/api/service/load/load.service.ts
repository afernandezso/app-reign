import { HttpCallsService } from '../../repository/http/httpCalls.service';
import { LoadHitMapper } from '../../mapper/loadHit.mapper';
import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ProcessRepositoryService } from '../../repository/mongoDB/process.repository';

/**
 *  service for data loading
 */
@Injectable()
export class LoadService {
  constructor(
    private httpCallsService: HttpCallsService,
    private processRepository: ProcessRepositoryService,
  ) {}

  /**
   * method to load data
   *
   */
  async loadData() {
    Logger.debug('LoadService loadData()');
    const loadResponse = (
      await this.httpCallsService.getDataAlgolia()
    ).toPromise();
    loadResponse.then(({ hits }) => {
      const loaderList = LoadHitMapper(hits);
      loaderList.forEach((loader) => {
        if (loader.url == null || loader.title == null) {
          Logger.debug(`the id ${loader.objectID} has no url or author`);
        } else {
          this.processRepository.create(loader);
        }
      });
    });
  }

  /**
   * cron is created to run every 1 hour
   */
  @Cron('0 */01 * * *')
  async loadDataCron() {
    Logger.debug('LoadService loadDataCron()');
   try {
     this.loadData();
   }catch (error) {
     Logger.error('Error loading data with cron:',error);
   }
  }
}
