## reign-api

backend services to load data from
```
 http://hn.algolia.com/api/v1/search_by_date?query=nodejs
```
these are stored in mongoDB.

LoadController
in charge of obtaining the information through an http call and storing it in mongoDB with the structure
```
{
       objectID: string,
       createdAt: string,
       title: string,
       url: string,
       author: string,
       state: boolean,
     }
```
ProcessController
in charge of obtaining and modifying data stored in mongodb.
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
`
