## app-reign
app built with nestjs + angular + mongoDB
## Description
The application has two components, server and client.

## Server
 Is responsible for obtaining data through an http call and espone an api to be consumed.

## Client
 It is a page where the list of loaded data is shown, it also has a functionality that allows you to delete records from the list. 

##  API methods 
data upload
```
curl --location --request GET 'http://localhost:3000/load'
```
list of data loaded with active status
```
curl --location --request GET 'http://localhost:3000/process/'
```

delete a record from the database
```
curl --location --request DELETE 'http://localhost:3000/process/{id}'
```

get a record from the database
```
curl --location --request GET 'http://localhost:3000/process/{id}'
```

clean all stored data, created to clean test data
```
curl --location --request DELETE 'http://localhost:3000/process/'
```
  
## Running the app
```
docker-compose up -d
```

## Initial data load
 load data by browser 
```
http://localhost:3000/load
```

or load using a curl call
```
curl --location --request GET 'http://localhost:3000/load'

```
##  page to view data
enter the loaded list [Web page](http://localhost/)

## Busy technologies documentation
 - [Docker](https://docs.docker.com/get-docker/)
 - [Docker-compose](https://docs.docker.com/compose/install/)
 - [Nodejs](https://nodejs.org/es/download/)
 - [Angular cli](https://angular.io/cli)
 - [Nestjs cli](https://docs.nestjs.com/)

## GitLab pipeline

  - build reign-api
  - run test reign-api
  - run test coverage reign-api
  - build web-app
