import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IProcess} from '../interfaces/process.interface';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) {}

  /**
   * get list of processes
   */
  getAll(): Observable<IProcess[]> {
    const headers = this.getHeaders();
    return this.http.get<IProcess[]>(environment.get_process, {headers});
  }

  /**
   * delete a record by id
   */
  delete(id: string): Observable<IProcess> {
    const headers = this.getHeaders();
    return this.http.delete<IProcess>(environment.delete_process + id, {headers});
  }

  /**
   * header
   */
  private getHeaders() {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
    });
    return headers;
  }
}
