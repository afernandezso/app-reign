import {Component, OnInit} from '@angular/core';
import {SearchService} from '../../services/search.service';
import {DatePipe} from '@angular/common';
import {IProcess} from '../../interfaces/process.interface';
import {constants} from "../../constants/constants";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchList: IProcess [] = [];

  constructor(private searchService: SearchService) {
    this.searchService.getAll()
      .subscribe((data: any) => {
        this.searchList = data;
        },error => {
        console.log('Failed to get the logs');
      });
  }

  ngOnInit(): void {
  }

  delete(objectID: string): void {
    this.searchService.delete(objectID).subscribe((data: IProcess ) => {
      if (!data.state) {
        this.searchList = this.searchList.filter((item: IProcess ) => item.objectID !== objectID);
      } else {
        console.log('Failed to delete the record');
      }
    }, error => {
      console.log('Failed to delete the record :',error);
    });
  }

  get sortData(): any {
    return this.searchList.sort((process: IProcess , processb: IProcess ) => {
      return new Date(this.getDateGMT(processb.createdAt)).getTime()- new Date (this.getDateGMT(process.createdAt)).getTime();
    });
  }

  private getDateGMT(value: string): string {
    return new DatePipe(constants.locale_us).transform(new Date(value).toISOString(), 'M/d/yy, h:mm', constants.time_zone)!;
  }

  showTrash(index: any): void {
    const element = document.getElementById(index);
    if (element != null) {
      element.setAttribute('class', 'showTrash');
    }
  }

  hideTrash(index: any): void {
    const element = document.getElementById(index);
    if (element != null) {
      element.setAttribute('class', 'hideTrash');
    }
  }

  linkUrl(url: string): void {
    window.open(url, '_blank');
  }
}
