import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe date author
 */
@Pipe({
  name: 'author'
})
export class AuthorPipe implements PipeTransform {

  transform(value: string): string {
    return '  - ' + value.replace('-', '') + ' -';
  }

}
