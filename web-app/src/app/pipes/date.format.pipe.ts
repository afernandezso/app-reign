import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';
import {constants} from "../constants/constants";

/**
 * Pipe date format
 */
@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {
  constructor(public datePipe: DatePipe) {}

  transform(value: string): string {
    const toDate = new Date(this.getDateFormatGMT(value));
    const newDate = new Date();
    if (toDate.getDate() === newDate.getDate()) {
      return <string> this.datePipe.transform(toDate, constants.format_h_mm_a)?.toLocaleLowerCase();
    } else if (toDate.getDay() === newDate.getDay() - 1) {
      return constants.format_yesterday;
    }
    return <string> this.datePipe.transform(toDate, constants.format_mmm_d);
  }


  private getDateFormatGMT(value: string): string {
    return new DatePipe(constants.locale_us).transform(new Date(value).toISOString(), constants.format_m_d_yy_h_mm_a, constants.time_zone)!;
  }
}
