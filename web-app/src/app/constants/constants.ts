export const constants = {
  format_mmm_d: 'MMM d',
  format_h_mm_a: 'h:mm a',
  format_m_d_yy_h_mm_a: 'M/d/yy, h:mm a',
  time_zone: 'GMT',
  locale_us: 'en-US',
  format_yesterday: 'Yesterday',
};
