export interface  IProcess {
  objectID: string;
  title: string;
  author: string;
  url: string;
  createdAt: string;
  state: boolean;
}
